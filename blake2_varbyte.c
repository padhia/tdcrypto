#define SQL_TEXT Latin_Text

#include <string.h>
#include <sqltypes_td.h>
#include "blake2.h"

void blake2_varbyte(VARBYTE* in, BYTEINT *len, VARBYTE* out, char sqlstate[6]) {
    blake2(out->bytes, out->length = *len, in->bytes, in->length, NULL, 0);
}
