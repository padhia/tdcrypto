#define SQL_TEXT Latin_Text

#include <string.h>
#include <sqltypes_td.h>
#include "blake2.h"

void blake2_bigint(BIGINT* in, BYTEINT* len, VARBYTE* out, char sqlstate[6]) {
    blake2(out->bytes, out->length = *len, in, SIZEOF_BIGINT, NULL, 0);
}
