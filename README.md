tdcrypto
========

Teradata cryptographic UDFs. Currently [BLAKE2](https://blake2.net/) is being used as the underlying cryptographic function.

Installation
------------

You'll need `CREATE FUNCTION` privilege on database or the user that'll own the UDFs.
You'll also need `ALTER FUNCTION` privilege if you want to alter the UDFs execution mode from protected to not protected.

- Switch to the repo directory.
- Set appropriate default database if UDFs need to be owned by a specific User or Database.
- If UDFs will be run in `PROTECTED` execution mode, edit `CryptoHash.sql` file and comment out the `ALTER FUNCTION` statements.
- Run `CryptoHash.sql` file.

Usage
-----

Note: The UDF parameter must be be either `VARCHAR` or `VARBYTE`.

```sql
SELECT InfoKey, InfoData, CryptoHash(InfoData) AS HashedData
  FROM DBC.DBCInfoV
```
